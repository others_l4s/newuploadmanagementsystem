/****** Object:  Table [dbo].[CommandErrorLogs]    Script Date: 24-10-2020 01:12:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommandErrorLogs](
	[ErrorLogId] [int] IDENTITY(1,1) NOT NULL,
	[ErrorLogDesc] [varchar](200) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ErrorLogId] PRIMARY KEY CLUSTERED 
(
	[ErrorLogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommandLogs]    Script Date: 24-10-2020 01:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommandLogs](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[LogDesc] [varchar](200) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_LogId] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Commands]    Script Date: 24-10-2020 01:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Commands](
	[CommandId] [int] IDENTITY(1,1) NOT NULL,
	[CommandType] [int] NOT NULL,
	[ZipPath] [varchar](200) NULL,
	[SiteId] [int] NULL,
	[ServerId] [int] NULL,
	[ServerName] [varchar](200) NULL,
	[Status] [tinyint] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ExecutedOn] [datetime] NULL,
	[IsReplace] [bit] NULL,
	[IsIgnoreRootFolder] [bit] NULL,
	[IsReplaceIgnoreFiles] [bit] NULL,
	[SiteName] [varchar](200) NULL,
 CONSTRAINT [PK_CommandId] PRIMARY KEY CLUSTERED 
(
	[CommandId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Servers]    Script Date: 24-10-2020 01:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Servers](
	[ServerId] [int] IDENTITY(1,1) NOT NULL,
	[ServerName] [varchar](200) NULL,
	[ServerDesc] [varchar](200) NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ServerId] PRIMARY KEY CLUSTERED 
(
	[ServerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ServerSites]    Script Date: 24-10-2020 01:12:26 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServerSites](
	[SiteId] [int] IDENTITY(1,1) NOT NULL,
	[ServerId] [int] NOT NULL,
	[SiteName] [varchar](200) NULL,
	[SiteDesc] [varchar](200) NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_SiteId] PRIMARY KEY CLUSTERED 
(
	[SiteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Commands] ADD  DEFAULT ((0)) FOR [IsReplace]
GO
ALTER TABLE [dbo].[Commands] ADD  DEFAULT ((0)) FOR [IsIgnoreRootFolder]
GO
ALTER TABLE [dbo].[Commands] ADD  DEFAULT ((0)) FOR [IsReplaceIgnoreFiles]
GO
