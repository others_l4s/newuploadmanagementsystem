# Deploy Files to Server

## Requirements
  - dotnet core hosting package for 3.1

## Configuration Steps

### 1. Run Website for Central Server
- Build Solution `DeployFilesToServer.sln`
- Set value in appsettings.json
    - `"BaseUrl": "http://localhost:56682"`- central server url
    - `"LoginUsername": "admin"` - website credentails
    - `"LoginPassword": "123"`
- Run website or Host website to IIS
- For Host to IIS
    - open command prompt in `DepoyFilesToServer\DeployFileWebApp` folder
    - Run `dotnet publish` command
    - host `bin\Debug\netcoreapp2.2\publish` folder to IIS
    - if you already have publish folder than directly host that folder to site
	- go to Manager server,
		- create Server that you want to configure. this should be unique.
		   - e.g. 182.168.0.125 - Development server/ UAT server
		- in installed windows service, use this server name in config file
		
### 2. Install Windows Service on Each server
- Create one folder name  `Logs`-
    - Create four folder under that
        - SiteBackupFolder

- Set key value in app.config.
    - Created folder path set in app.config
    - `<add key="ConnectionString" value=""/>`
    - `<add key="SiteBackupFolderPath" value="E:\DeployZip\SiteBackupFolder"/>`
    - `<add key="ServerName" value="182.168.0.125"/>` - Created server in website
- Build Solution `DeployFilesToServer.sln`
- Copy `DepoyFilesToServer\DeployFilesWindowsService\bin\Debug` to server
    e.g `C:\HostedFiles\UploadFilesManagment\WindowsService`
- If you already have build folder then you directly skip above steps and in that folder you can see DeployFilesWindowsService.exe.config and change folder path in that config file.
- Open cmd prompt with run as administrator
- Type below command with windows service exe path
- `sc create DeployFilesService binpath= "C:\HostedFiles\UploadFilesManagment\WindowsService\DeployFilesWindowsService.exe"`
- `sc start DeployFilesService`
- Start service from `services.msc`

### 3. Upload Files to Server steps
 - After running website, you can see hoated website list
 - select multiple website of different server at a time
 - create zip of your changes files and select in upload zip
 - then click upload
 - After sometimes you will see logs in below Logs listing grid