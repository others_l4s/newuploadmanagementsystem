﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Data
{
	[Table("Servers")]
	public class Servers
	{
		[Key]
		public int ServerId { get; set; }
		public string ServerName { get; set; }
		public string ServerDesc { get; set; }
		public DateTime CreatedOn { get; set; }
	}
}
