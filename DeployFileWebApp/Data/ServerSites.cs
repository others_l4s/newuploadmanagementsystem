﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Data
{
	[Table("ServerSites")]
	public class ServerSites
	{
		[Key]
		public int SiteId { get; set; }
		public int ServerId { get; set; }
		public string SiteName { get; set; }
		public string SiteDesc { get; set; }
		public DateTime CreatedOn { get; set; }

		[Computed]
		public string ServerName { get; set; }
	}
}
