﻿using Dapper.Contrib.Extensions;
using System;

namespace DeployFileWebApp.Data
{
	[Table("Commands")]
	public class Commands
	{
		[Key]
		public int CommandId { get; set; }
		public int CommandType { get; set; }
		public string ZipPath { get; set; }
		public int SiteId { get; set; }
		public string SiteName { get; set; }
		public int ServerId { get; set; }
		public string ServerName { get; set; }
		public int Status { get; set; }
		public bool IsReplace { get; set; }
		public bool IsIgnoreRootFolder { get; set; }
		public bool IsReplaceIgnoreFiles { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? ExecutedOn { get; set; }
	}
}
