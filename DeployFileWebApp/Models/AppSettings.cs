﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Models
{
	public class AppSettings
	{
		public string BaseUrl { get; set; }
		public string LoginUsername { get; set; }
		public string LoginPassword { get; set; }
	}
}
