﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Models
{
	public class ServerModel
	{
		public int ServerId { get; set; }

		[Required(ErrorMessage = "Server Name is required")]
		public string ServerName { get; set; }
		public string ServerDesc { get; set; }
	}
}
