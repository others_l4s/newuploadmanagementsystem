﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Models
{
    public class ApiErrorModel
    {
        public bool Success { get; set; }
        public string ReturnMsg { get; set; }
        public List<string> Errors { get; set; }

        public ApiErrorModel(string error)
        {
            this.Errors = new List<string>
            {
                error
            };
            this.ReturnMsg = error;
            this.Success = false;
        }

        public ApiErrorModel(ModelStateDictionary modelState)
        {
            this.Success = false;
            if (modelState?.Any(m => m.Value.Errors.Count > 0) == true)
            {
                this.Errors = modelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
                string message = string.Join("<br/>", this.Errors);
                this.ReturnMsg = "Validation Failed:" + message;
            }
        }

        public ApiErrorModel()
        {
        }
    }
}
