﻿using DeployFileWebApp.Data;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeployFileWebApp.Models
{
	public class DataModel
	{
		[Required(ErrorMessage = "Site is required")]
		public List<int> SiteIds { get; set; }

		[Required(ErrorMessage = "File is required")]
		//[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.zip)$", ErrorMessage = "Only .zip extension files are allowed.")]
		public IFormFile File { get; set; }
		public List<CommandLogs> Logs { get; set; }
		public List<CommandErrorLogs> ErrorLogs { get; set; }

		public bool IsReplace { get; set; }
		public bool IsIgnoreRootFolder { get; set; }

		public bool IsReplaceIgnoreFiles { get; set; }
	}
}
