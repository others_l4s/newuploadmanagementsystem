﻿using DeployFileWebApp.Data;
using DeployFileWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFileWebApp.Repository
{
	public interface IDatabaseRepository
	{
		bool SaveCommand(List<Commands> commands);
		Servers GetServer(int serverId);
		bool SaveServer(ServerModel serverModel);
		List<Servers> GetAllServers();

		List<ServerSites> GetAllSites();

		List<ServerSites> GetAllSites(int serverId);

		List<ServerSites> GetAllSites(string siteids);

		List<CommandLogs> GetAllLogs();

		List<CommandErrorLogs> GetAllErrorLogs();
	}
}
