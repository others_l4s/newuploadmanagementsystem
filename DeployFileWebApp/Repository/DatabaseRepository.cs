﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using DeployFileWebApp.Data;
using DeployFileWebApp.Models;
using Microsoft.Extensions.Configuration;

namespace DeployFileWebApp.Repository
{
	public class DatabaseRepository : IDatabaseRepository
	{
		private readonly string connectionString = string.Empty;
		public SqlConnection con;
		public DatabaseRepository(IConfiguration configuration)
		{
			connectionString = configuration.GetConnectionString("DefaultConnection");
		}
		private void Connection()
		{
			con = new SqlConnection(connectionString);
		}

		public bool SaveServer(ServerModel serverModel)
		{
			try
			{
				if (serverModel.ServerId > 0)
				{

					var server = GetServer(serverModel.ServerId);
					server.ServerName = serverModel.ServerName;
					server.ServerDesc = serverModel.ServerDesc;
					con.Update(server);
				}
				else
				{
					var server = new Servers()
					{
						ServerName = serverModel.ServerName,
						ServerDesc = serverModel.ServerDesc,
						CreatedOn = DateTime.Now
					};
					var identity = con.Insert(server);
				}
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public Servers GetServer(int serverId)
		{
			try
			{
				Connection();
				con.Open();
				var servers = SqlMapper.Query<Servers>(con, @"SELECT * From Servers where ServerId = @serverId", new { serverId });
				con.Close();
				return servers.ToList().FirstOrDefault();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}
		public bool SaveCommand(List<Commands> commands)
		{
			try
			{
				var identity = con.Insert(commands);
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return false;
		}

		public List<Servers> GetAllServers()
		{
			try
			{
				Connection();
				con.Open();
				var servers = SqlMapper.Query<Servers>(con, @"SELECT * From Servers");
				con.Close();
				return servers.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}

		public List<ServerSites> GetAllSites()
		{
			try
			{
				Connection();
				con.Open();
				var sites = SqlMapper.Query<ServerSites>(con, @"SELECT SS.SiteId SiteId, SS.SiteName SiteName, S.ServerName ServerName, S.ServerId ServerId From ServerSites SS inner join Servers S on SS.ServerId = S.ServerId");
				con.Close();
				return sites.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}

		public List<ServerSites> GetAllSites(string siteids)
		{
			try
			{
				Connection();
				con.Open();
				var sites = SqlMapper.Query<ServerSites>(con, @"SELECT SS.SiteId SiteId, SS.SiteName SiteName, S.ServerName ServerName, S.ServerId ServerId From ServerSites SS inner join Servers S on SS.ServerId = S.ServerId where SS.SiteId in (" + siteids + ")");
				con.Close();
				return sites.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}

		public List<ServerSites> GetAllSites(int serverId)
		{
			try
			{
				Connection();
				con.Open();
				var sites = SqlMapper.Query<ServerSites>(con, @"SELECT SS.SiteId SiteId, 
								SS.SiteName SiteName, S.ServerName ServerName, S.ServerId ServerId, SS.CreatedOn CreatedOn From 
						ServerSites SS inner join Servers S on SS.ServerId = S.ServerId where SS.ServerId = @serverId", new { serverId });
				con.Close();
				return sites.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}
		public List<CommandLogs> GetAllLogs()
		{
			try
			{
				Connection();
				con.Open();
				var logs = SqlMapper.Query<CommandLogs>(con, @"SELECT top 20 * From CommandLogs order by createdon desc");
				con.Close();
				return logs.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}

		public List<CommandErrorLogs> GetAllErrorLogs()
		{
			try
			{
				Connection();
				con.Open();
				var logs = SqlMapper.Query<CommandErrorLogs>(con, @"SELECT top 20 * From CommandErrorLogs order by createdon desc");
				con.Close();
				return logs.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}
	}
}
