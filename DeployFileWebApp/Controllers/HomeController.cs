﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DeployFileWebApp.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using System.IO;
using CommonService;
using DeployFileWebApp.Repository;
using Microsoft.AspNetCore.Hosting;
using DeployFileWebApp.Data;

namespace DeployFileWebApp.Controllers
{
	public class HomeController : Controller
	{
		private readonly AppSettings _appSettings;
		private const string _loginSessionKeyName = "_Username";

		private readonly ILogger<HomeController> _logger;
		private readonly IDatabaseRepository _databaseRepository;
		private readonly IWebHostEnvironment _webHostEnvironment;
		public HomeController(ILogger<HomeController> logger, IOptions<AppSettings> options, IDatabaseRepository databaseRepository, IWebHostEnvironment webHostEnvironment)
		{
			_appSettings = options.Value;
			_logger = logger;
			_databaseRepository = databaseRepository;
			_webHostEnvironment = webHostEnvironment;
		}

		public IActionResult Index()
		{
            var username = HttpContext.Session.GetString(_loginSessionKeyName);
            if (string.IsNullOrEmpty(username))
            {
                return RedirectToAction("Index", "Login");
            }

            ViewBag.ListOfSites = _databaseRepository.GetAllSites();

			ViewData["IsLogin"] = true;

			var model = new DataModel()
			{
				Logs = _databaseRepository.GetAllLogs(),
				ErrorLogs = _databaseRepository.GetAllErrorLogs(),
				IsIgnoreRootFolder = true
			};
			return View(model);
		}

		public IActionResult Privacy()
		{
			return View();
		}

		[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
		public IActionResult Error()
		{
			return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
		}

		public ActionResult Logout()
		{
			HttpContext.Session.Remove(_loginSessionKeyName);
			return RedirectToAction("Index", "Login");
		}

		[HttpPost]
		[RequestFormLimits(MultipartBodyLengthLimit = 209715200)]
		[RequestSizeLimit(209715200)]
		public ActionResult UploadZip(DataModel dataModel)
		{
			if (ModelState.IsValid)
			{
				if (dataModel.File != null)
				{
					var file = dataModel.File;
					var extension = file.FileName.Split(".").Last().ToLower();
					var fileName = DateTime.Now.ToFileTime() + "." + extension;
					string filePath = Path.Combine(_webHostEnvironment.WebRootPath, "Upload", fileName);
					using (FileStream fs = System.IO.File.Create(filePath))
					{
						file.CopyTo(fs);
						fs.Flush();
					}
					var fileUrlPath = _appSettings.BaseUrl + "/Upload/" + fileName;

					var siteDetailsList = _databaseRepository.GetAllSites(string.Join(',', dataModel.SiteIds));
					var commands = new List<Commands>();
					foreach (var siteId in dataModel.SiteIds)
					{
						var siteDetails = siteDetailsList.Where(x => x.SiteId == siteId).FirstOrDefault();
						commands.Add(new Commands
						{
							CommandType = (int)CommandTypes.UploadFiles,
							IsIgnoreRootFolder = dataModel.IsIgnoreRootFolder,
							IsReplace = dataModel.IsReplace,
							IsReplaceIgnoreFiles = dataModel.IsReplaceIgnoreFiles,
							ServerId = siteDetails.ServerId,
							SiteId = siteId,
							ServerName = siteDetails.ServerName,
							ZipPath = fileUrlPath,
							Status = (int)CommandStatus.Pending,
							SiteName = siteDetails.SiteName,
							CreatedOn = DateTime.Now
						});
					}
					var isSuccess = _databaseRepository.SaveCommand(commands);
					if (isSuccess)
					{
						return this.GetJsonResult("File upload command sent successfully. Please wait for few minutes.");
					}
					return this.GetErrorJsonResult("Please try again later!");
				}
				return this.GetErrorJsonResult("Please select zip file");
			}
			else
			{
				return this.GetJsonResult();
			}
		}

		//[HttpPost]
		//public ActionResult UpdateSiteList()
		//{
		//	PutCommand((int)Commands.UpdateSiteList);
		//	return RedirectToAction("Index", "Home");
		//}


		[HttpGet]
		public ActionResult GetLogList()
		{
			return StatusCode(200, _databaseRepository.GetAllLogs());
		}

		[HttpGet]
		public ActionResult GetErrorLogs()
		{
			return StatusCode(200, _databaseRepository.GetAllErrorLogs());
		}
			
	}
}
