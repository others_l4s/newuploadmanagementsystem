﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DeployFileWebApp.Models;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Http;
using System.IO;
using CommonService;
using DeployFileWebApp.Repository;
using Microsoft.AspNetCore.Hosting;
using DeployFileWebApp.Data;

namespace DeployFileWebApp.Controllers
{
	public class ManageServerController : Controller
	{
		private const string _loginSessionKeyName = "_Username";
		private readonly IDatabaseRepository _databaseRepository;
		public ManageServerController(IDatabaseRepository databaseRepository, IWebHostEnvironment webHostEnvironment)
		{
			_databaseRepository = databaseRepository;
		}

		public IActionResult Index()
		{
            var username = HttpContext.Session.GetString(_loginSessionKeyName);
            if (string.IsNullOrEmpty(username))
            {
                return RedirectToAction("Index", "Login");
            }
			ViewData["IsLogin"] = true;
			var servers = _databaseRepository.GetAllServers();
			return View(servers);
		}

		public IActionResult Sites(int id)
		{
			//var username = HttpContext.Session.GetString(_loginSessionKeyName);
			//if (string.IsNullOrEmpty(username))
			//{
			//	return RedirectToAction("Index", "Login");
			//}
			var sites = _databaseRepository.GetAllSites(id);
			return View(sites);
		}

		[HttpGet]
		public IActionResult GetServers()
		{
			var servers = _databaseRepository.GetAllServers();
			return PartialView("_ServerList", servers);
		}

		[HttpGet]
		public IActionResult SaveServer(int id = 0)
		{
			ServerModel model = new ServerModel();
			try
			{
				if (id != 0)
				{
					var data = _databaseRepository.GetServer(id);
					model = new ServerModel()
					{
						ServerId = data.ServerId,
						ServerName = data.ServerName,
						ServerDesc = data.ServerDesc
					};
				}
				return PartialView(model);
			}
			catch (Exception ex)
			{
				return PartialView(model);
			}
		}

		[HttpPost]
		public IActionResult SaveServer(ServerModel model)
		{
			if (ModelState.IsValid)
			{
				var response = _databaseRepository.SaveServer(model);
				return this.GetJsonResult("Server Saved Successfully");
			}
			else
			{
				return this.GetJsonResult();
			}
		}

		[HttpPost]
		public IActionResult UpdateSite(Servers server)
		{
			var commands = new List<Commands>();
			commands.Add(new Commands
			{
				CommandType = (int)CommandTypes.UpdateSiteList,
				ServerId = server.ServerId,
				SiteId = 0,
				ServerName = server.ServerName,
				ZipPath = string.Empty,
				Status = (int)CommandStatus.Pending,
				CreatedOn = DateTime.Now
			});
			var isSuccess = _databaseRepository.SaveCommand(commands);
			if (isSuccess)
			{
				return this.GetJsonResult("Update site command sent successfully. Please wait for few minutes.");
			}
			return this.GetErrorJsonResult("Please try again later!");
		}
	}
}
