﻿using CommonService;
using Microsoft.Web.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.ServiceProcess;
using CommonService.Extensions;
using DeployFilesWindowsService.Data;
using System.Timers;
using System.Net;

namespace DeployFilesWindowsService
{
	public partial class Service1 : ServiceBase
	{
		private bool IsAnyCommandInExecution = false;
		public Service1()
		{
			InitializeComponent();

			//if (!System.Diagnostics.EventLog.SourceExists("AfterSalesLogSourse"))
			//	System.Diagnostics.EventLog.CreateEventSource("AfterSalesLogSourse", "AfterSalesLog");

			//EventLog.Source = "AfterSalesLogSourse";
			//EventLog.Log = "AfterSalesLog";
		}

		protected override void OnStart(string[] args)
		{
			GetAllSiteNames();
			{
				Timer timerEach5Sec = new Timer();
				timerEach5Sec.Enabled = true;
				timerEach5Sec.Interval = (1000 * 5);    //Execute every 5sec
				timerEach5Sec.Elapsed += new System.Timers.ElapsedEventHandler(ProcessCommands);
			}
		}

		protected override void OnStop()
		{

		}

		#region ' Private Methods'
		//Process Commands
		private void ProcessCommands(object sender, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				if (IsAnyCommandInExecution)
					return;
				var db = new DatabaseRepository();
				var commands = db.GetAllCommands(ConfigurationManager.AppSettings["ServerName"]);
				foreach (var command in commands)
				{
					IsAnyCommandInExecution = true;
					var isSuccess = false;
					db.UpdateCommandStatus(command.CommandId, (int)CommandStatus.Started);
					if (command.CommandType == (int)CommandTypes.UploadFiles)
						isSuccess = UploadFolderToSite(command);
					else if (command.CommandType == (int)CommandTypes.UpdateSiteList)
						isSuccess = GetAllSiteNames();
					if (isSuccess)
					{
						db.UpdateCommandStatus(command.CommandId, (int)CommandStatus.Completed);
					}
					else
					{
						db.UpdateCommandStatus(command.CommandId, (int)CommandStatus.Error);
					}
				}
				IsAnyCommandInExecution = false;
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
		}

		//Upload Files to folder
		private bool UploadFolderToSite(Commands command)
		{
			var isSuccess = false;
			try
			{
				using (ServerManager iisManager = new ServerManager())
				{
					string siteNameListString = string.Empty;
					Site site = iisManager.Sites[command.SiteName];
					if (site != null)
					{
						try
						{
							site.Stop();
							string uploadPath = site.Applications["/"].VirtualDirectories["/"].PhysicalPath;
							//Take backup of Hosted folder
							var currentDateTime = DateTime.Now;
							string backupZipName = currentDateTime.Year + "_" + currentDateTime.Month + "_" + currentDateTime.Day + " " + currentDateTime.Hour + "_" + currentDateTime.Minute + "_" + currentDateTime.Second + ".zip";
							string backpZipPath = Path.Combine(ConfigurationManager.AppSettings["SiteBackupFolderPath"], command.SiteName);
							if (!Directory.Exists(backpZipPath))
							{
								Directory.CreateDirectory(backpZipPath);
							}
							backpZipPath = Path.Combine(backpZipPath, backupZipName);
							ZipFile.CreateFromDirectory(uploadPath, backpZipPath);

							//Upload new files
							var ignoreFileList = GetIgnoreFileNames();
							if (command.IsReplace)
							{
								DeleteDirectory(uploadPath, true, command.IsReplaceIgnoreFiles, ignoreFileList);
							}

							Uri uri = new Uri(command.ZipPath);
							string filename = System.IO.Path.GetFileName(uri.LocalPath); ;

							var baseFolder = AppDomain.CurrentDomain.BaseDirectory + @"\ZipFile\" + filename;

							if (!File.Exists(baseFolder))
							{
								using (WebClient web1 = new WebClient())
								{
									web1.DownloadFile(command.ZipPath, baseFolder);
								}
							}

							using (var archive = ZipFile.Open(baseFolder, ZipArchiveMode.Read))
							{
								archive.ExtractToDirectory(uploadPath, true, ignoreFileList, command.IsIgnoreRootFolder, command.IsReplaceIgnoreFiles);
							}
							Log($"{command.SiteName} latest uploaded successfully");
							isSuccess = true;
						}
						catch (Exception ex)
						{
							ErrorLog(ex.Message.ToString());
						}
						finally
						{
							site.Start();
						}
					}
				}
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
			return isSuccess;
		}

		//Get All Site name
		private bool GetAllSiteNames()
		{
			var isSuccess = false;
			try
			{
				var db = new DatabaseRepository();
				var serverId = db.GetServerId(ConfigurationManager.AppSettings["ServerName"]);
				if (serverId < 1)
					return isSuccess;

				var siteNames = new List<string>();
				using (ServerManager iisManager = new ServerManager())
				{
					var siteNameListString = string.Empty;
					foreach (var site in iisManager.Sites)
					{
						siteNames.Add(site.Name);
					}
				}

				foreach (var item in siteNames)
				{
					db.SaveSite(item, serverId);
				}
				Log($"Site list updated successfully");
				isSuccess = true;
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
			return isSuccess;
		}

		//Save execution log
		private void Log(string text)
		{
			var db = new DatabaseRepository();
			db.SaveLog(new CommandLogs()
			{
				CreatedOn = DateTime.Now,
				LogDesc = ConfigurationManager.AppSettings["ServerName"] + "-" + text
			});
		}

		//Save error log
		private void ErrorLog(string text)
		{
			var db = new DatabaseRepository();
			db.SaveErrorLog(new CommandErrorLogs()
			{
				CreatedOn = DateTime.Now,
				ErrorLogDesc = ConfigurationManager.AppSettings["ServerName"] + "-" + text
			});
		}

		//Save Site name list to text file
		private void UpdateSiteNameList(string text)
		{
			var sitenameListFilePath = Path.Combine(ConfigurationManager.AppSettings["LogFolderFilePath"], "sitenamelist.txt");
			if (File.Exists(sitenameListFilePath))
			{
				File.Delete(sitenameListFilePath);
			}
			File.AppendAllText(sitenameListFilePath, text);
		}

		//Get Ignore File name

		private List<string> GetIgnoreFileNames()
		{
			List<string> list = new List<string>();
			try
			{
				list = File.ReadAllLines(AppDomain.CurrentDomain.BaseDirectory + @"\IgnoreFiles.txt").Select(x => x.Trim()).ToList();
			}
			catch (Exception ex)
			{
				ErrorLog(ex.Message.ToString());
			}
			return list;
		}


		private void DeleteDirectory(string target_dir, bool isParent, bool isReplaceIgnoreFiles, List<string> ignoreFiles)
		{
			string[] files = Directory.GetFiles(target_dir);
			string[] dirs = Directory.GetDirectories(target_dir);

			foreach (string file in files)
			{
				var fullName = file.Split('\\').Last();
				if (!isReplaceIgnoreFiles && ignoreFiles.Contains(fullName))
				{
					continue;
				}
				File.SetAttributes(file, FileAttributes.Normal);
				File.Delete(file);
			}

			foreach (string dir in dirs)
			{
				DeleteDirectory(dir, false, isReplaceIgnoreFiles, ignoreFiles);
			}

			if (!isParent)
			{
				if (!Directory.EnumerateFileSystemEntries(target_dir).Any())
				{
					Directory.Delete(target_dir);
				}
			}
		}
		#endregion
	}
}
