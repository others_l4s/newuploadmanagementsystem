﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFilesWindowsService.Data
{
	[Table("CommandErrorLogs")]
	public class CommandErrorLogs
	{
		[Key]
		public int ErrorLogId { get; set; }
		public string ErrorLogDesc { get; set; }
		public DateTime CreatedOn { get; set; }
	}
}
