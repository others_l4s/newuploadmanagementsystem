﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeployFilesWindowsService.Data
{
	[Table("CommandLogs")]
	public class CommandLogs
	{
		[Key]
		public int LogId { get; set; }
		public string LogDesc { get; set; }
		public DateTime CreatedOn { get; set; }
	}
}
