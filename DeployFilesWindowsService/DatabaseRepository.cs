﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using CommonService;
using Dapper;
using Dapper.Contrib.Extensions;
using DeployFilesWindowsService.Data;

namespace DeployFilesWindowsService
{
	public class DatabaseRepository
	{
		private readonly string connectionString = string.Empty;
		public SqlConnection con;
		public DatabaseRepository()
		{
			connectionString = ConfigurationManager.AppSettings["ConnectionString"];
			con = new SqlConnection(connectionString);
		}

		public int GetServerId(string serverName)
		{
			try
			{
				con.Open();
				string getQuery = @"Select Top 1 ServerId from Servers where ServerName = @serverName";
				var serverId = con.Query<int>(getQuery, new
				{
					serverName
				}).FirstOrDefault();
				return serverId;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				con.Close();
			}
			return 0;
		}

		public bool SaveSite(string siteName, int serverId)
		{
			try
			{
				con.Open();
				string updateQuery = @"IF NOT EXISTS (SELECT * FROM ServerSites WHERE ServerId = @serverId and SiteName = @siteName) 
										INSERT INTO ServerSites values(@serverId, @siteName, @siteName, GETDATE())";
				var result = con.Execute(updateQuery, new
				{
					siteName,
					serverId
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				con.Close();
			}
			return false;
		}
		public bool SaveLog(CommandLogs commandLog)
		{
			try
			{
				var identity = con.Insert(commandLog);
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return false;
		}

		public bool SaveErrorLog(CommandErrorLogs commandLog)
		{
			try
			{
				var identity = con.Insert(commandLog);
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			return false;
		}

		public bool UpdateCommandStatus(int commandId, int status)
		{
			try
			{
				con.Open();
				string updateQuery = @"UPDATE Commands SET Status = @status WHERE CommandId = @commandId";
				if (status == (int)CommandStatus.Completed || status == (int)CommandStatus.Error)
					updateQuery = @"UPDATE Commands SET Status = @status, ExecutedOn=Getdate()  WHERE CommandId = @commandId";
				var result = con.Execute(updateQuery, new
				{
					status,
					commandId
				});
				return true;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				con.Close();
			}
			return false;
		}

		public List<Commands> GetAllCommands(string serverName)
		{
			try
			{
				con.Open();
				var commands = SqlMapper.Query<Commands>(con, @"SELECT * From Commands where ServerName = '" + serverName + "' and status = 0");
				return commands.ToList();
			}
			catch
			{
				throw;
			}
			finally
			{
				con.Close();
			}
		}
	}
}
