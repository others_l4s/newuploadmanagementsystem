﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace CommonService.Extensions
{
	public static class ZipArchiveExtensions
	{
		public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryFullPath, bool overwrite, List<string> ignoreFiles, bool isIgnoreRootFolder, bool isReplaceIgnoreFiles)
		{
			if (!overwrite)
			{
				archive.ExtractToDirectory(destinationDirectoryFullPath);
				return;
			}

			foreach (ZipArchiveEntry file in archive.Entries)
			{
				var fileName = file.FullName;
				if (isIgnoreRootFolder)
				{
					fileName = file.FullName.Substring(file.FullName.IndexOf("/") + 1);
				}
				string completeFileName = Path.GetFullPath(Path.Combine(destinationDirectoryFullPath, fileName));

				if (!completeFileName.StartsWith(destinationDirectoryFullPath, StringComparison.OrdinalIgnoreCase))
				{
					throw new IOException("Trying to extract file outside of destination directory. See this link for more info: https://snyk.io/research/zip-slip-vulnerability");
				}

				if (file.Name == "")
				{
					// Assuming Empty for Directory
					Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
					continue;
				}

				var directoryName = Path.GetDirectoryName(completeFileName);
				if (!Directory.Exists(directoryName))
				{
					Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
				}

				if (!isReplaceIgnoreFiles && ignoreFiles.Contains(file.Name))
				{
					continue;
				}
				file.ExtractToFile(completeFileName, true);
			}
		}
	}
}
