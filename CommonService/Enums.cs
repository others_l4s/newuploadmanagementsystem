﻿namespace CommonService
{
	public enum CommandTypes
	{
		UploadFiles,
		UpdateSiteList
	}

	public enum CommandStatus
	{
		Pending,
		Started,
		Completed,
		Error
	}
}
